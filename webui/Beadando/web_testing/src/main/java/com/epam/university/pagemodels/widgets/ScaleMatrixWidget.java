package com.epam.university.pagemodels.widgets;

import com.epam.university.pagemodels.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class ScaleMatrixWidget extends BasePage{

    private By layer = null;
    private String layerPath;
    private int _questionNumber;


    public ScaleMatrixWidget(WebDriver driver, int questionNumber) {
        super(driver);
        _questionNumber = questionNumber;
        layerPath = "//div[@class='__question__ office-form-question  '][" + _questionNumber + "]";
        layer = By.xpath(layerPath);
    }

    public WebElement[][] GetButtons(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        List<WebElement> rows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(layerPath+"/div/div/div['office-form-matrix-group']//div[@class='office-form-matrix-row']")));
        List<WebElement> fields = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(layerPath+"/div/div/div['office-form-matrix-group']//div[@class='office-form-matrix-row'][1]/div[@tabindex='-1']/input")));
        WebElement[][] btns = new WebElement[rows.size()][fields.size()];
        for (int i = 0; i < fields.size(); ++i){
            btns[0][i] = fields.get(i);
        }
        for (int i = 2; i <= rows.size(); ++i){
            fields = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(layerPath+"/div/div/div['office-form-matrix-group']//div[@class='office-form-matrix-row']["+i+"]/div[@tabindex='-1']/input")));
            for (int j = 0; j < fields.size(); ++j){
                btns[i-1][j] = fields.get(j);
            }
        }
        return btns;
    }

    public void TickRadio(int row, int col){
        GetButtons()[row][col].click();
    }

    public int GetSelectedInLine(int lineIndex){
        int selected = 0;
        while(selected < this.GetButtons()[lineIndex].length && !this.GetButtons()[lineIndex][selected].isSelected()){
            ++selected;
        }
        if(selected >= this.GetButtons()[lineIndex].length){
            return -1;
        }
        return selected;
    }

}
