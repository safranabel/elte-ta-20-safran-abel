package com.epam.university.pagemodels.widgets;

        import com.epam.university.pagemodels.pages.BasePage;
        import com.epam.university.pagemodels.pages.EvaluationPage;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.Select;
        import org.openqa.selenium.support.ui.WebDriverWait;

        import java.util.List;

public class OnAScaleTableWidget extends BasePage {
    private By layer = null;
    private int _questionNumber;
    private String layerPath;

    public OnAScaleTableWidget(WebDriver driver, int questionNumber) {
        super(driver);
        _questionNumber = questionNumber;
        layerPath = "//div[@class='__question__ office-form-question  '][" + _questionNumber + "]";
        layer = By.xpath(layerPath);
    }

    public void TickRadio(int index) {
        WebElement tickEr = GetButtons().get(index);
        tickEr.click();
    }

    public int GetSelected(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        List<WebElement> tableFields = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(layerPath+"/div/div[2]/div/div/table/tbody/tr/td/div/input")));
        int selected = 0;
        while(selected < GetButtons().size() && !tableFields.get(selected).isSelected()){
            ++selected;
        }
        if(selected >= GetButtons().size()){
            return -1;
        }
        return selected;
    }

    public List<WebElement> GetButtons(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        List<WebElement> buttons =wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(layerPath+"/div/div[2]/div/div/table/tbody/tr/td")));
        return buttons;
    }
}