package com.epam.university.pagemodels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SuccessPage extends BasePage {
    private By successMessage = By.xpath("//span[@class='thank-you-page-comfirm-text']");
    private  By successIcon = By.xpath("//i[@class='ms-Icon ms-Icon--Completed office-form-theme-primary-color thank-you-page-font-icon']");

    public SuccessPage(WebDriver driver) {
        super(driver);
    }

    public boolean isSuccessMessageDisplayed(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement successMsg = wait.until(ExpectedConditions.presenceOfElementLocated(this.successMessage));

        boolean isSuccessMessageDisplayed = successMsg != null;
        return isSuccessMessageDisplayed;
    }

    public boolean isSuccessIconDisplayed(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement successIco = wait.until(ExpectedConditions.presenceOfElementLocated(this.successIcon));

        boolean isSuccessIconDisplayed = successIco != null;
        return isSuccessIconDisplayed;
    }
}
