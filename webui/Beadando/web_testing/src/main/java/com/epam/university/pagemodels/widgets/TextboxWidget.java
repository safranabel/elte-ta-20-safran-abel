package com.epam.university.pagemodels.widgets;

import com.epam.university.pagemodels.pages.BasePage;
import com.epam.university.pagemodels.pages.EvaluationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TextboxWidget extends BasePage {

    private By layer = null;
    private int _questionNumber;
    private By inputField = null;

    public TextboxWidget(WebDriver driver, int questionNumber) {
        super(driver);
        _questionNumber = questionNumber;
        String layerPath = "//div[@class='__question__ office-form-question  '][" + _questionNumber + "]";
        layer = By.xpath(layerPath);
        inputField = By.xpath(layerPath + "/div/div[2]/div/div/input");
    }


    public void writeAnswer(String answer) {
        WebElement textField = driver.findElement(inputField);

        textField.clear();
        textField.sendKeys(answer);
    }

}
