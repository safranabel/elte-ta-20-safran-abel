package com.epam.university.pagemodels.pages;

import com.epam.university.pagemodels.widgets.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class EvaluationPage extends BasePage {
    private ScaleMatrixWidget firstQuestion;
    private RadioButtonsWidget secondQuestion;
    private TextboxWidget thirdQuestion;
    private OnAScaleTableWidget fourthQuestion;
    private By title;


    public EvaluationPage(WebDriver driver) {
        super(driver);
        firstQuestion = new ScaleMatrixWidget(driver, 1);
        secondQuestion = new RadioButtonsWidget(driver, 2);
        thirdQuestion = new TextboxWidget(driver, 3);
        fourthQuestion = new OnAScaleTableWidget(driver, 4);
        title = By.xpath("//*[@id='form-container']/div/div/div/div/div[1]/div[2]/div[2]/div[1]/div/div/div/div/span");
    }

    public ScaleMatrixWidget GetFirstQuestion(){
        return firstQuestion;
    }

    public RadioButtonsWidget GetSecondQuestion(){
        return secondQuestion;
    }

    public TextboxWidget GetThirdQuestion(){ return thirdQuestion; }

    public OnAScaleTableWidget GetFourthQuestion(){ return fourthQuestion; }

    public void Next(){
        WebElement btn = driver.findElement(By.xpath("//button[@class='office-form-theme-primary-background office-form-theme-button office-form-bottom-button button-control light-background-button __submit-button__']"));
        if(btn!=null){
            btn.click();
        }
    }

    public void Previous(){
        WebElement btn = driver.findElement(By.xpath("//button[@class='office-form-bottom-button button-control light-background-button office-form-theme-secondary-color gray-background section-button']"));
        if(btn!=null){
            btn.click();
        }
    }
    public String GetTitle(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement titleSpan = wait.until(ExpectedConditions.presenceOfElementLocated(title));
        return  titleSpan.getText();
    }
}
