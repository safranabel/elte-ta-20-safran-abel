package com.epam.university.pagemodels.pages;

import com.epam.university.pagemodels.widgets.OnAScaleRadioWidget;
import com.epam.university.pagemodels.widgets.TextboxWidget;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ExpectationsPage extends BasePage {
    private TextboxWidget firstQuestion;
    private OnAScaleRadioWidget secondQuestion;
    private By title;

    public ExpectationsPage(WebDriver driver) {
        super(driver);
        firstQuestion = new TextboxWidget(driver, 1);
        secondQuestion = new OnAScaleRadioWidget(driver, 2);
        title = By.xpath("//*[@id='form-container']/div/div/div/div/div[1]/div[2]/div[2]/div[1]/div/div/div/div/span");
    }

    public TextboxWidget GetFirstQuestion(){
        return firstQuestion;
    }

    public OnAScaleRadioWidget GetSecondQuestion(){
        return secondQuestion;
    }

    public void Next(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement btn = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@class='office-form-theme-primary-background office-form-theme-button office-form-bottom-button button-control light-background-button section-next-button section-button']")));
        if(btn!=null){
            btn.click();
        }
    }

    public String GetTitle(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement titleSpan = wait.until(ExpectedConditions.presenceOfElementLocated(title));
        return  titleSpan.getText();
    }

}
