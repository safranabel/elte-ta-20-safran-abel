package com.epam.university.tests;

import com.epam.university.pagemodels.pages.ExpectationsPage;
import com.epam.university.pagemodels.pages.EvaluationPage;
import com.epam.university.pagemodels.pages.SuccessPage;
import com.epam.university.pagemodels.widgets.TextboxWidget;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnitParamsRunner.class)
public class BeadandoPageObjectTest extends TestBase{
    @Test
    @FileParameters("src/main/resources/tdd/MyAnswers.csv")
    public void FillTheFormsTest(String answer1, String answer2, String answer3a, String answer3b, String answer3c, String answer3d, String answer4, String answer5, String answer6) throws IOException {
        ExpectationsPage firstPage = new ExpectationsPage(driver);
        firstPage.navigateToTestAutomationFeedback(driver);
        Assert.assertTrue(firstPage.GetTitle().equals("Elvárások"));

        answer1 = resolveTextAnswer(answer1);
        firstPage.GetFirstQuestion().writeAnswer(answer1);

        int answer2toIndex = resolveIndexAnswer(answer2);

        if(answer2toIndex > -1){
            firstPage.GetSecondQuestion().TickRadio(answer2toIndex);
        }
        Assert.assertEquals(answer2toIndex, firstPage.GetSecondQuestion().GetSelected());

        firstPage.Next();


        EvaluationPage secondPage = new EvaluationPage(driver);
        Assert.assertTrue(secondPage.GetTitle().equals("Kurzus értékelése"));

        // radio matrix answers
        int answer3atoIndex = resolveIndexAnswer(answer3a);
        if(answer3atoIndex > -1){
            secondPage.GetFirstQuestion().TickRadio(0, answer3atoIndex);
        }
        Assert.assertEquals(answer3atoIndex, secondPage.GetFirstQuestion().GetSelectedInLine(0));

        int answer3btoIndex = resolveIndexAnswer(answer3b);
        if(answer3btoIndex > -1){
            secondPage.GetFirstQuestion().TickRadio(1, answer3btoIndex);
        }
        Assert.assertEquals(answer3btoIndex, secondPage.GetFirstQuestion().GetSelectedInLine(1));

        int answer3ctoIndex = resolveIndexAnswer(answer3c);
        if(answer3ctoIndex > -1){
            secondPage.GetFirstQuestion().TickRadio(2,answer3ctoIndex);
        }
        Assert.assertEquals(answer3ctoIndex, secondPage.GetFirstQuestion().GetSelectedInLine(2));

        int answer3dtoIndex = resolveIndexAnswer(answer3d);
        if(answer3dtoIndex > -1){
            secondPage.GetFirstQuestion().TickRadio(3, answer3dtoIndex);
        }
        Assert.assertEquals(answer3dtoIndex, secondPage.GetFirstQuestion().GetSelectedInLine(3));

        // radio buttons answer
        int answer4toIndex = resolveIndexAnswer(answer4);
        if(answer4toIndex > -1){
            secondPage.GetSecondQuestion().TickRadio(answer4toIndex);
        }
        Assert.assertEquals(answer4toIndex, secondPage.GetSecondQuestion().GetSelected());


        //input textbox
        answer5 = resolveTextAnswer(answer5);
        secondPage.GetThirdQuestion().writeAnswer(answer5);


        //table radio
        int answer6toIndex = resolveIndexAnswer(answer6);
        if(answer6toIndex > -1){
            secondPage.GetFourthQuestion().TickRadio(answer6toIndex);
        }
        Assert.assertEquals(answer6toIndex, secondPage.GetFourthQuestion().GetSelected());


        secondPage.Next();

        SuccessPage lastPage = new SuccessPage(driver);
        Assert.assertTrue(lastPage.isSuccessMessageDisplayed());
        Assert.assertTrue(lastPage.isSuccessIconDisplayed());

    }

    private String resolveTextAnswer(String answer){
        if(answer.equals("Empty") || answer.equals("empty")){
            return "";
        }

        StringBuilder answerBuilder = new StringBuilder(answer);
        for (int i = 0; i < answer.length(); ++i){
            if(answerBuilder.charAt(i) == ';'){
                answerBuilder.setCharAt(i,',');
            }
        }
        return answerBuilder.toString();
    }

    private int resolveIndexAnswer(String answer){
        int index = -1;
        if(!answer.equals("Empty") && !answer.equals("empty")){
            index = Integer.parseInt(answer) - 1;
        }
        return index;
    }
}
