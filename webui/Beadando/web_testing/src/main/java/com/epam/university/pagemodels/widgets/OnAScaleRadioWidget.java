package com.epam.university.pagemodels.widgets;

import com.epam.university.pagemodels.pages.BasePage;
import com.epam.university.pagemodels.pages.EvaluationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class OnAScaleRadioWidget extends BasePage{
    private By layer = null;
    private String layerPath;
    private int _questionNumber;

    public OnAScaleRadioWidget(WebDriver driver, int questionNumber) {
        super(driver);
        _questionNumber = questionNumber;
        layerPath = "//div[@class='__question__ office-form-question  '][" + _questionNumber + "]";
        layer = By.xpath(layerPath);
    }


    public void TickRadio(int index) {

        List<WebElement> buttons = this.GetButtons();
        WebElement tickEr = buttons.get(index);
        tickEr.click();
    }


    public int GetSelected(){
        int selected = 0;
        while(selected < this.GetButtons().size() && !this.GetButtons().get(selected).isSelected()){
            ++selected;
        }
        if(selected >= this.GetButtons().size()){
            return -1;
        }
        return selected;
    }

    public List<WebElement> GetButtons(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        List<WebElement> buttons =wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(layerPath+"//div[2]/div/div/div/input")));
        return buttons;
    }
}









